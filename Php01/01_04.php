<?php
    // Datatyper og variabler. Dette dokument indlejres i det efterfølgende

    //tekststreng (string)
    $firstName = "Thor";

    //tekststreng (string)
    $lastName = "Kousted";

    //nummerisk heltal (integer)
    $age = 24;

    //boolean (sandt/falsk)
    $inRelationship = True;

    //tekststreng
    $work = "Studerende";

    //tekststreng
    $workPlace = "Erhvervsakademi Dania";

    //array (en række)
    $hobbies = ["PC","Kodning","Sove"];